-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Machine: 127.0.0.1
-- Gegenereerd op: 03 jan 2016 om 21:43
-- Serverversie: 5.6.21
-- PHP-versie: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databank: `infra_sec_db`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `route`
--

CREATE TABLE IF NOT EXISTS `route` (
`id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `steps` int(11) NOT NULL,
  `time` varchar(45) NOT NULL,
  `user_id` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `route`
--

INSERT INTO `route` (`id`, `name`, `steps`, `time`, `user_id`, `dateCreated`) VALUES
(1, 'Route', 0, '0:05:251', 1, '2016-01-03 17:11:49'),
(2, 'Route', 124, '0:05:943', 1, '2016-01-03 17:12:10'),
(3, 'Route', 23, '0:05:943', 1, '2016-01-03 17:12:10'),
(4, 'Route', 0, '0:02:934', 1, '2016-01-03 20:01:09'),
(5, 'Route', 0, '0:05:626', 1, '2016-01-03 21:06:25');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `route_location`
--

CREATE TABLE IF NOT EXISTS `route_location` (
`id` int(11) NOT NULL,
  `lng` varchar(45) NOT NULL,
  `lat` varchar(45) NOT NULL,
  `route_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `route_location`
--

INSERT INTO `route_location` (`id`, `lng`, `lat`, `route_id`) VALUES
(1, '4.9909977', '52.3589085', 1),
(2, '4.9909977', '52.3589085', 1),
(3, '4.9909977', '52.3589085', 1),
(4, '4.9909977', '52.3589085', 1),
(5, '4.9909977', '52.3589085', 1),
(6, '4.9909977', '52.3589085', 1),
(7, '4.9909977', '52.3589085', 2),
(8, '4.9909977', '52.3589085', 2),
(9, '4.9909977', '52.3589085', 2),
(10, '4.9909977', '52.3589085', 2),
(11, '4.9909977', '52.3589085', 2),
(12, '4.9909977', '52.3589085', 2),
(13, '4.9909977', '52.3589085', 3),
(14, '4.9909977', '52.3589085', 3),
(15, '4.9909977', '52.3589085', 3),
(16, '4.9909977', '52.3589085', 3),
(17, '4.9909977', '52.3589085', 3),
(18, '4.9909977', '52.3589085', 3),
(19, '4.9913425', '52.3589039', 4),
(20, '4.9913425', '52.3589039', 4),
(21, '4.9913425', '52.3589039', 4),
(22, '4.9913425', '52.3589039', 5),
(23, '4.9913425', '52.3589039', 5),
(24, '4.9913425', '52.3589039', 5),
(25, '4.9913425', '52.3589039', 5),
(26, '4.9913425', '52.3589039', 5),
(27, '4.9913425', '52.3589039', 5);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`id` int(11) NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `displayName` varchar(45) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `displayName`) VALUES
(1, 'ezra', 'admin', 'Ezra Waalboer'),
(2, '1', '1', '1'),
(4, 'JsonName', 'JsonPass', 'JsonDiplayName');

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `route`
--
ALTER TABLE `route`
 ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `route_location`
--
ALTER TABLE `route_location`
 ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `route`
--
ALTER TABLE `route`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT voor een tabel `route_location`
--
ALTER TABLE `route_location`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT voor een tabel `user`
--
ALTER TABLE `user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
