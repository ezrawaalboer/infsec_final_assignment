package Model;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Ezra Waalboer
 */
@XmlRootElement
public class RouteLocation {

    private int id;
    private String lat;
    private String lng;
    private int route_id;

    public RouteLocation() {
    }

    public RouteLocation(String latitude, String longitude, int route_id) {
        this.lat = latitude;
        this.lng = longitude;
        this.route_id = route_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLatitude() {
        return lat;
    }

    public void setLatitude(String latitude) {
        this.lat = latitude;
    }

    public String getLongitude() {
        return lng;
    }

    public void setLongitude(String longitude) {
        this.lng = longitude;
    }

    public int getRoute_id() {
        return route_id;
    }

    public void setRoute_id(int route_id) {
        this.route_id = route_id;
    }

   

    @Override
    public String toString() {
        return "RouteLocation{" + "id=" + id + ", latitude=" + lat + ", longitude=" + lng + ", route_id=" + route_id + '}';
    }

    
    
    
    
    
    
    
    
}
