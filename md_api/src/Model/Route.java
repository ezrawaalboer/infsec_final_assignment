package Model;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import org.codehaus.jettison.json.JSONArray;

/**
 * @author Ezra Waalboer
 */
@XmlRootElement
public class Route {

    private int id;
    private String name;
    private int steps;
    private String time;
    private int user_id;
    private List<String> latList;
    private List<String> longList;
    private String dateCreated;

    public Route() {
    }

    public Route(String name, int steps, String time, int user_id, List<String> latList, List<String> longList, String dateCreated) {
        this.name = name;
        this.steps = steps;
        this.time = time;
        this.user_id = user_id;
        this.latList = latList;
        this.longList = longList;
        this.dateCreated = dateCreated;
    }
    
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSteps() {
        return steps;
    }

    public void setSteps(int steps) {
        this.steps = steps;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public List<String> getLatList() {
        return latList;
    }

    public void setLatList(List<String> latList) {
        this.latList = latList;
    }

    public List<String> getLongList() {
        return longList;
    }

    public void setLongList(List<String> longList) {
        this.longList = longList;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    @Override
    public String toString() {
        return "Route{" + "id=" + id + ", name=" + name + ", steps=" + steps + ", time=" + time + ", user_id=" + user_id + ", latList=" + latList + ", longList=" + longList + ", dateCreated=" + dateCreated + '}';
    }
    
    
    
    
}
