package Resource;

import DAO.RouteDAO;
import DAO.RouteLocationDAO;
import Model.Route;
import Model.RouteLocation;
import Model.User;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * @author Ezra Waalboer
 */
@Path("/route_gps")
public class RouteLocationResource {

    RouteLocationDAO dao = new RouteLocationDAO();

    @POST
    @Path("details")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces({MediaType.APPLICATION_JSON})
    public List<RouteLocation> findAllByRouteId(Route route) {
        return dao.findAllByRouteId(route.getId());
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces({MediaType.APPLICATION_JSON})
    public RouteLocation findById(RouteLocation routeLocation) {
        return dao.findById(routeLocation.getId());
    }

    @POST
    @Path("create")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces({MediaType.APPLICATION_JSON})
    public RouteLocation createNewLocation(RouteLocation routeLocation) {
        return dao.create(routeLocation);
    }
   
}
