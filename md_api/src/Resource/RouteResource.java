package Resource;

import DAO.RouteDAO;
import DAO.RouteLocationDAO;
import Model.Route;
import Model.RouteLocation;
import Model.User;
import static com.sun.corba.se.spi.presentation.rmi.StubAdapter.request;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * @author Ezra Waalboer
 */
@Path("/route")
public class RouteResource {

    RouteDAO dao = new RouteDAO();
    RouteLocationDAO locationDoa = new RouteLocationDAO();

    @POST
    @Path("user")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces({MediaType.APPLICATION_JSON})
    public List<Route> findAllByUserId(User user) {
        return dao.findAllByUserId(user.getId());
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces({MediaType.APPLICATION_JSON})
    public Route findById(Route route) {
        return dao.findById(route.getId());
    }

    @POST
    @Path("create")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces({MediaType.APPLICATION_JSON})
    public Route createRoute(Route route) {
        System.out.println("route info: " + route.toString());
        return dao.create(route);
        
//        return route;
    }
   
}
