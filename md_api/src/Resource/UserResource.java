package Resource;

import DAO.RouteLocationDAO;
import DAO.UserDAO;
import Model.User;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * @author Ezra Waalboer
 */
@Path("/user")
public class UserResource {

    UserDAO userDAO = new UserDAO();
    RouteLocationDAO locationDAO = new RouteLocationDAO();
    RouteLocationDAO routeDAO = new RouteLocationDAO();

    @POST
    @Path("register")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces({MediaType.APPLICATION_JSON})
    public User createUser(User user) {
        User create = new User(user.getUsername(), user.getPassword(), user.getDisplayName());
        return userDAO.create(create);
    }
    
    @POST
    @Path("login")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces({MediaType.APPLICATION_JSON})
    public User loginUser(User user) {
        return userDAO.findUserByUserPassword(user.getUsername(), user.getPassword());
    }
    
}
