package Connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;

/**
 * @author Ezra Waalboer
 */
public class ConnectionManager {

    private String url;
    private static ConnectionManager instance;
    private final static String DATABASE_NAME = "infra_sec_db";

    private ConnectionManager() {
        String driver = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            url = "jdbc:mysql://localhost/" + DATABASE_NAME;
            ResourceBundle bundle = ResourceBundle.getBundle(DATABASE_NAME);
            driver = bundle.getString("jdbc.driver");
            Class.forName(driver);
            url = bundle.getString("jdbc.url");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Connection getConnection() throws SQLException {
        if (instance == null) {
            instance = new ConnectionManager();
        }
        try {
            return DriverManager.getConnection(instance.url, "root", "");
        } catch (SQLException e) {
            throw e;
        }
    }

    public static void close(Connection connection) {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
