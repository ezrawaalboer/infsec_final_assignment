package DAO;

import Connection.ConnectionManager;
import Model.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ezra Waalboer
 */
public class UserDAO {

    public User findUserByUserPassword(String username, String password) {
        String sql = "SELECT e.* FROM user as e "
                + "WHERE e.username = ? "
                + "AND e.password = ? ";
        User user = null;
        Connection c = null;
        try {
            c = ConnectionManager.getConnection();
            PreparedStatement ps = c.prepareStatement(sql);
            ps.setString(1, username);
            ps.setString(2, password);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                user = processRow(rs);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } finally {
            ConnectionManager.close(c);
        }
        return user;
    }

    public User save(User user) {
        return user.getId() > 0 ? update(user) : create(user);
    }

    public User create(User user) {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = ConnectionManager.getConnection();
            ps = c.prepareStatement("INSERT INTO user (username, password, displayName) VALUES (?, ?, ?)",
                    new String[]{"ID"});
            ps.setString(1, user.getUsername());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getDisplayName());
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            rs.next();
            // Update the id in the returned object. This is important as this value must be returned to the client.
            int id = rs.getInt(1);
            user.setId(id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } finally {
            ConnectionManager.close(c);
        }
        return user;
    }

    public User update(User user) {
        Connection c = null;
        try {
            c = ConnectionManager.getConnection();
            PreparedStatement ps = c.prepareStatement("UPDATE user SET username=?, password=?, displayName=? WHERE id=?");
            ps.setString(1, user.getUsername());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getDisplayName());
            ps.setInt(11, user.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } finally {
            ConnectionManager.close(c);
        }
        return user;
    }

    public boolean remove(User user) {
        Connection c = null;
        try {
            c = ConnectionManager.getConnection();
            PreparedStatement ps = c.prepareStatement("DELETE FROM user WHERE id=?");
            ps.setInt(1, user.getId());
            int count = ps.executeUpdate();
            return count == 1;
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } finally {
            ConnectionManager.close(c);
        }
    }

    protected User processRow(ResultSet rs) throws SQLException {
        User user = new User();
        user.setId(rs.getInt("id"));
        user.setUsername(rs.getString("username"));
        user.setPassword(rs.getString("password"));
        user.setDisplayName(rs.getString("displayName"));
        return user;
    }


}
