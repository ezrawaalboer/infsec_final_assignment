package DAO;

import Connection.ConnectionManager;
import Model.Route;
import Model.RouteLocation;
import Model.User;
import java.io.Console;
import static java.lang.System.console;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import jdk.nashorn.internal.parser.JSONParser;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 * @author Ezra Waalboer
 */
public class RouteDAO {

        public List<Route> findAllByUserId(int userId) {
        List<Route> list = new ArrayList<Route>();
        String sql = "SELECT e.* FROM route as e "
                + "WHERE e.user_id = ? ";
        Connection c = null;
        try {
            c = ConnectionManager.getConnection();
            PreparedStatement ps = c.prepareStatement(sql);
            ps.setInt(1, userId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(processRow(rs));
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } finally {
            ConnectionManager.close(c);
        }
        return list;
    }

    public Route findById(int id) {
        String sql = "SELECT e.* FROM route as e "
                + "WHERE e.id = ? ";
        Route route = null;
        Connection c = null;
        try {
            c = ConnectionManager.getConnection();
            PreparedStatement ps = c.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                route = processRow(rs);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } finally {
            ConnectionManager.close(c);
        }
        return route;
    }
    
    public Route findByUserId(int user_id) {
        String sql = "SELECT e.* FROM route as e "
                + "WHERE e.user_id = ? ";
        Route route = null;
        Connection c = null;
        try {
            c = ConnectionManager.getConnection();
            PreparedStatement ps = c.prepareStatement(sql);
            ps.setInt(1, user_id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                route = processRow(rs);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } finally {
            ConnectionManager.close(c);
        }
        return route;
    }
    

    public Route save(Route route) {
        return route.getId() > 0 ? update(route) : create(route);
    }

    public Route create(Route route) {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = ConnectionManager.getConnection();
            ps = c.prepareStatement("INSERT INTO route (name, steps, user_id, time) VALUES (?, ?, ?, ?)",
                    new String[]{"ID"});
            ps.setString(1, route.getName());
            ps.setInt(2, route.getSteps());
            ps.setInt(3, route.getUser_id());
            ps.setString(4, route.getTime());
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            rs.next();
            // Update the id in the returned object. This is important as this value must be returned to the client.
            int id = rs.getInt(1);
            route.setId(id);
            
            if(id > 0){
                System.out.println("size lat: " + route.getLatList().size());
                createRouteLocation(route);
            }
            
            
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } finally {
            ConnectionManager.close(c);
        }
        return route;
    }

    public Route update(Route route) {
        Connection c = null;
        try {
            c = ConnectionManager.getConnection();
            PreparedStatement ps = c.prepareStatement("UPDATE route SET name=?, steps=?, user_id=? WHERE id=?");
            ps.setString(1, route.getName());
            ps.setInt(2, route.getSteps());
            ps.setInt(3, route.getUser_id());
            ps.setInt(11, route.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } finally {
            ConnectionManager.close(c);
        }
        return route;
    }

    public boolean remove(Route route) {
        Connection c = null;
        try {
            c = ConnectionManager.getConnection();
            PreparedStatement ps = c.prepareStatement("DELETE FROM route WHERE id=?");
            ps.setInt(1, route.getId());
            int count = ps.executeUpdate();
            return count == 1;
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } finally {
            ConnectionManager.close(c);
        }
    }

    protected Route processRow(ResultSet rs) throws SQLException {
        Route route = new Route();
        route.setId(rs.getInt("id"));
        route.setName(rs.getString("name"));
        route.setSteps(rs.getInt("steps"));
        route.setUser_id(rs.getInt("user_id"));
        route.setTime(rs.getString("time"));
        route.setDateCreated(rs.getString("dateCreated"));
        return route;
    }
    
    
     public void createRouteLocation(Route route){
                  
         List latitude = route.getLatList();
         List longitude = route.getLongList();
       
         RouteLocationDAO locationDAO = new RouteLocationDAO(); 
        
         for(int i = 0; i < latitude.size(); i++){
             RouteLocation location = new RouteLocation(latitude.get(i).toString(), longitude.get(i).toString(), route.getId());
             
             System.out.println(location.toString());
             
                 locationDAO.create(location); 
            
//             
//             System.out.println("lat value: " + latitude.get(i));
//             System.out.println("lng value: " + longitude.get(i));
         }
         
     }
}
