package DAO;

import Connection.ConnectionManager;
import Model.Route;
import Model.RouteLocation;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ezra Waalboer
 */
public class RouteLocationDAO {

  
    public RouteLocation findById(int id) {
        String sql = "SELECT e.* FROM route_location as e "
                + "WHERE e.id = ? ";
        RouteLocation route = null;
        Connection c = null;
        try {
            c = ConnectionManager.getConnection();
            PreparedStatement ps = c.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                route = processRow(rs);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } finally {
            ConnectionManager.close(c);
        }
        return route;
    }
    
    public List<RouteLocation> findAllByRouteId(int route_id) {
        List<RouteLocation> list = new ArrayList<RouteLocation>();
        String sql = "SELECT e.* FROM route_location as e "
                + "WHERE e.route_id = ? ";
        RouteLocation route = null;
        Connection c = null;
        try {
            c = ConnectionManager.getConnection();
            PreparedStatement ps = c.prepareStatement(sql);
            ps.setInt(1, route_id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(processRow(rs));
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } finally {
            ConnectionManager.close(c);
        }
        return list;
    }
    

    public RouteLocation save(RouteLocation route) {
        return route.getId() > 0 ? update(route) : create(route);
    }

     public RouteLocation create(RouteLocation route) {
        Connection c = null;
        PreparedStatement ps = null;
        try {
            c = ConnectionManager.getConnection();
            ps = c.prepareStatement("INSERT INTO route_location (lng, lat, route_id) VALUES (?, ?, ?)",
                    new String[]{"ID"});
            ps.setString(1, route.getLongitude());
            ps.setString(2, route.getLatitude());
            ps.setInt(3, route.getRoute_id());
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            rs.next();
            // Update the id in the returned object. This is important as this value must be returned to the client.
            int id = rs.getInt(1);
            route.setId(id);
                        
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } finally {
            ConnectionManager.close(c);
        }
        return route;
    }

    public RouteLocation update(RouteLocation route) {
        Connection c = null;
        try {
            c = ConnectionManager.getConnection();
            PreparedStatement ps = c.prepareStatement("UPDATE route_location SET lat=?, long=?, route_id=? WHERE id=?");
            ps.setString(1, route.getLatitude());
            ps.setString(2, route.getLongitude());
            ps.setInt(3, route.getRoute_id());
            ps.setInt(11, route.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } finally {
            ConnectionManager.close(c);
        }
        return route;
    }

    public boolean remove(RouteLocation route) {
        Connection c = null;
        try {
            c = ConnectionManager.getConnection();
            PreparedStatement ps = c.prepareStatement("DELETE FROM route WHERE id=?");
            ps.setInt(1, route.getId());
            int count = ps.executeUpdate();
            return count == 1;
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } finally {
            ConnectionManager.close(c);
        }
    }

    protected RouteLocation processRow(ResultSet rs) throws SQLException {
        RouteLocation route = new RouteLocation();
        route.setId(rs.getInt("id"));
        route.setLatitude(rs.getString("lat"));
        route.setLongitude(rs.getString("lng"));
        route.setRoute_id(rs.getInt("route_id"));
        return route;
    }

}
